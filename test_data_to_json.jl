# Transforms the test data into JSON form

#= Imports =#

import SHA
import TOML
import JSON

#= Constants =#

"""
The location fo the test data file.
"""
const SOURCE_PATH = joinpath(@__DIR__, "test_data.toml")

"""
Path of the generated JSON file.
"""
const TARGET_PATH = joinpath(@__DIR__, "test_data_as.json")

"""
This will be used as the institution in the test credentials.
"""
const TAU_NAME = "Tampere University"

"""
A description of Tampere University.
"""
const TAU_DESCRIPTION = "TAU conducts scientific research in technology and" *
    " architecture and provides the highest education within these fields."

"""
A hash of the name of Tampere University.
"""
const TAU_HASH = SHA.bytes2hex(SHA.sha256("Tampere University"))

"""
A hash of the assumed credential owner.
"""
const SANTTU_HASH = SHA.bytes2hex(SHA.sha256("Santtu Söderholm"))

"""
This is not an actual wallet hash.
"""
const ETHEREUM_HASH = SHA.bytes2hex(SHA.sha256("Ethereum wallet"))

"""
These values we do not really care about, but we set (to null) them for
completeness' sake.
"""
const DEFAULT_ACHIEVEMENT_KEYS = [
    ("altLabel", nothing),
    ("refLanaguage", nothing),
    ("dependentAchievementIds", []),
    ("field", nothing),
    ("eqfLevel", nothing),
    ("nqfLevel", nothing),
    ("creditSystem", "ECTS"),
    ("creditSysTitle", "ECTS"),
    ("creditSysDef", nothing),
    ("creditSysValue", "1"),
    ("creditSysIssuer", nothing),
    ("canConsistOfIds", []),
    ("creditSysRefNum", nothing),
    ("numCreditPoints", "1"),
    ("ectsCreditPoints", "1"),
    ("volumeOfLearning", nothing),
    ("isPartialQual", nothing),
    ("waysToAcquire", nothing),
    ("eduCredType", nothing),
    ("entryReq", nothing),
    ("learningOutcome", nothing),
    ("relatedOccupation", nothing),
    ("recognition", nothing),
    ("awardingActivity", nothing),
    ("awardingMethod", nothing),
    ("gradeScheme", nothing),
    ("modeOfStudy", "Online"),
    ("publicKey", nothing),
    ("assesmentMethod", nothing),
    ("accreditation", nothing),
    ("homePage", nothing),
    ("landingPage", nothing),
    ("supplDoc", nothing),
    ("historyNote", nothing),
    ("additionalNote", nothing),
    ("status", nothing),
    ("replacesId", nothing),
    ("replacedById", nothing),
    ("owner", nothing),
    ("creator", nothing),
    ("_updatedAt", "2021-06-09T06:03:38.876Z"),
    ("_createdAt", "2021-06-08T23:13:46.297Z")
]

const DEFAULT_CREDENTIAL_VALUES = [
    ("stage", 4),
    ("note", nothing),
    ("grade", nothing),
    ("awardingBodyId", nothing),
    ("creditsAwarded", nothing),
    ("expiryPeriod", nothing),
    ("cheating", nothing),
    ("actionsOrderId", nothing),
    ("metadata", nothing),
    ("txHash", nothing),
    ("_createdAt", "2021-06-11T08,06,54.172Z")

]

#= Types =#
abstract type ProgramResult <: Any end
struct ProgramSuccess <: ProgramResult end
struct TOMLParseFailure <: ProgramResult end

#= Helper functions =#

"""
Normalizes a given string: removes surrounding whitespace,
turns multiple whitespaces into a single one and capitalizes
the first letter in the string while making every other letter
lower-case.
"""
function normalize(string::String)
    uppercasefirst(join(split(strip(string), isspace), " "))
end

#= The Program =#

"""
Transforms the data in the `SOURCE_PATH` to JSON form.
"""
function main()
    # Read source into Julia data structures
    data_parsing_result = TOML.tryparsefile(SOURCE_PATH)
    if typeof(data_parsing_result) == TOML.ParserError
        println("Error at (row, column) =" *
            " ($(data_parsing_result.line), $(data_parsing_result.column))" *
            " when parsing `test_data.toml`…")
        return TOMLParseFailure
    end
    # Generate credential objects
    credentials = []
    for (index, credential) ∈ enumerate(data_parsing_result["test_credentials"])
        # Initialize credential
        test_credential = Dict()
        # Build credential ID from achievement name and index
        namestring    = credential["name"] * string(index, base=10)
        credential_id = SHA.bytes2hex(SHA.sha256(namestring))
        test_credential["id"] = credential_id
        test_credential["profileId"] = SANTTU_HASH
        test_credential["wallet"] = "0x" * ETHEREUM_HASH
        for (key, value) ∈ DEFAULT_CREDENTIAL_VALUES
            test_credential[key] = value
        end
        # Build underlying achievement
        achievement = Dict()
        achievement["id"] = SHA.bytes2hex(SHA.sha256(credential["name"]))
        achievement["name"] = credential["name"]
        achievement["tag"] = [tag for tag in credential["tags"]]
        institution_hash = SHA.bytes2hex(
            SHA.sha256(credential["issuing_institution"]))
        achievement["communityIds"] = [ institution_hash ]
        for (key, value) ∈ DEFAULT_ACHIEVEMENT_KEYS
            achievement[key] = value
        end
        achievement["publisherId"] = institution_hash
        test_credential["achievement"] = achievement
        # Build associated communities
        communities = [
            Dict("id" => TAU_HASH, "name" => TAU_NAME,
                 "description" => TAU_DESCRIPTION, "keys" => [])]
        test_credential["communities"] = communities
        # Add names of credentials this credential depends on
        test_credential["dependentAchievementIds"] = credential["dependencies"]
        # Finally, add credential to constructed credentials
        push!(credentials, test_credential)
    end

    open(TARGET_PATH,"w") do f
        JSON.print(f, credentials, 4)
    end
    return ProgramSuccess
end

main()
